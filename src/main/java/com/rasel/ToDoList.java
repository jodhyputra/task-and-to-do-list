package com.rasel;

import java.util.ArrayList;

public class ToDoList {
    private static ArrayList<String> todolist = new ArrayList<>();

    public void insertToDoList(int id, String name, String status) {
        Task task = new Task();

        task.insertTaskData(id, name, status);
        todolist.add(task.getTaskData());
    }

    public static String getToDoList() {
        StringBuilder tempList = new StringBuilder();

        for (int i = 0; i < todolist.size(); i++) {
            if (i < todolist.size() - 1) {
                tempList.append(todolist.get(i)).append("\n");
            } else {
                tempList.append(todolist.get(i));
            }
        }
        return tempList.toString();
    }

    public static void addtoDoList(){
        ToDoList todolist = new ToDoList();

        todolist.insertToDoList(1, "Do Dishes", "DONE");
        todolist.insertToDoList(2, "Learn Java", "DONE");
        todolist.insertToDoList(3, "Learn TDD", "NOT DONE");
    }

    public static String changetoDoList(int numSearch){
        String info = "";
        String tempList = "";

        if (numSearch-1 < todolist.size()){
            tempList = todolist.get(numSearch-1);
        }

        if(tempList.contains("NOT DONE")){
            todolist.set(numSearch-1, tempList.replace("NOT DONE", "DONE"));
            info = "Task updated";
        }
        else if (tempList.contains("DONE")){
            info = "Task already done!";
        }
        else if (numSearch-1 >= todolist.size()){
            info = "ID is not valid";
        }
        return info;
    }
}
