package com.rasel;

public class Task {
    public int id;
    public String name;
    public String status;

    public void insertTaskData(int id, String name, String status) {
        this.id = id;
        this.name = name;
        this.status = status;
    }

    public String getTaskData() {
        if (name == null) {
            return null;
        }
        return id + ". " + name + " " + "[" + status + "]";
    }

    public String getStatusTask() {
        if (status != "DONE" && status != "NOT DONE") {
            return "Status must between DONE or NOT DONE";
        }
        return status;
    }
}
