package com.rasel;

import java.util.Scanner;

public class Main {
    public static void menu(){
        System.out.println("1. View All to Do List");
        System.out.println("2. Update a to Do List");
        System.out.println("0. Exit");
    }

    public static void main(String[] args) {
        int inputMenu;
        int inputUpdate;
        Scanner myScan = new Scanner(System.in);

        ToDoList.addtoDoList();

        do {
            menu();
            System.out.println("Input: ");
            inputMenu = myScan.nextInt();

            switch (inputMenu){
                case 0:
                    System.out.println("Thanks for use our feature");
                    break;

                case 1:
                    System.out.println("View List");
                    System.out.println(ToDoList.getToDoList());
                    break;

                case 2:
                    System.out.println("Update a List");
                    System.out.println(ToDoList.getToDoList());
                    System.out.println("Which number?");
                    inputUpdate = myScan.nextInt();
                    System.out.println(ToDoList.changetoDoList(inputUpdate));
                    break;

                default:
                    System.out.println("Not valid number");
                    break;
            }
        }while (inputMenu != 0);
    }
}