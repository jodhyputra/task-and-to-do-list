package com.rasel;

import org.junit.Test;

import static org.junit.Assert.*;

public class TaskTest {
    @Test
    public void taskDataIsCorrectTest() {
        String expectedTask = "1. Do Dishes [DONE]";

        Task task = new Task();

        task.insertTaskData(1, "Do Dishes", "DONE");
        assertEquals(expectedTask, task.getTaskData());
    }

    @Test
    public void taskIsNullTest() {
        Task task = new Task();
        assertNull(task.getTaskData());
    }

    @Test
    public void taskStatusCorrectTest() {
        Task task = new Task();
        String expectedStatus = "Status must between DONE or NOT DONE";

        assertEquals(expectedStatus, task.getStatusTask());
    }
}
