package com.rasel;

import org.junit.Test;

import static org.junit.Assert.*;

public class ToDoListTest {
    @Test
    public void addToDoListTest() {
        String expectedTodo = "1. Do Dishes [DONE]\n2. Learn Java [DONE]\n3. Learn TDD [NOT DONE]";

        ToDoList todolist = new ToDoList();

        todolist.insertToDoList(1, "Do Dishes", "DONE");
        todolist.insertToDoList(2, "Learn Java", "DONE");
        todolist.insertToDoList(3, "Learn TDD", "NOT DONE");

        String actual = todolist.getToDoList();

        assertEquals(expectedTodo, actual);
    }

    @Test
    public void changeToDoListTest(){
        String expectedTodo = "1. Do Dishes [DONE]\n2. Learn Java [DONE]\n3. Learn TDD [DONE]";

        ToDoList todolist = new ToDoList();

        todolist.insertToDoList(1, "Do Dishes", "DONE");
        todolist.insertToDoList(2, "Learn Java", "DONE");
        todolist.insertToDoList(3, "Learn TDD", "NOT DONE");

        ToDoList.changetoDoList(3);
        String actual = todolist.getToDoList();

        assertEquals(expectedTodo, actual);
    }
}